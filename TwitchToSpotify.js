// Create server and launch interface

// import all modules
global._ = require('./bin/main');

const express = _.tools.express;
const cors = _.tools.cors;
const cookieParser = _.tools.cookieParser;
const bodyParser = _.tools.bodyParser;
const querystring = _.tools.querystring;
let io = _.tools.io;
const http = _.tools.http;

const spotify = _.spotify;
const twitch = _.twitch;
const appRoute = _.appRoute;
const socket = _.socket;

// Setting server and io socket

let app = express();

app.use('/', express.static(__dirname + '/views/'))
   .use('/css', express.static(__dirname + '/public/css/'))
   .use('/js', express.static(__dirname + '/public/js/'))
   .use(cors())                // Cross-origin resource sharing
   .use(cookieParser())        // Parse Cookie header and populate req.cookies
   .use(bodyParser.json())    // support json encoded bodies
   .use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

let server = http.Server(app);
io = io(server);

// Define socket usage
socket.setupOnEvent(io);

// Add user informations
const clientId = 'e34730dbbdf14076a544d7f34958dcf4'; // Your client id
const clientSecret = 'e398ec71f624424c9e942360d3188fd5'; // Your secret
const redirectUri = 'http://localhost:8888/callback_spotify'; // Your redirect uri

_.user = {
    clientId: clientId,
    clientSecret: clientSecret,
    redirectUri: redirectUri
};

// Dict used for stock search result of users
_.searchDict = {};

// Setting route

spotify.loginRoute(app);
spotify.callbackRoute(app);

appRoute.appRoute(app);

// Launch listening on paort 8888
console.log('Listening on 8888');
server.listen(8888);

// Open nav
const url = 'http://localhost:8888/';
const start = (process.platform == 'darwin'? 'open': process.platform == 'win32'? 'start': 'xdg-open');
require('child_process').exec(start + ' ' + url);

// ---------------------
