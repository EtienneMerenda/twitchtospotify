const twitchButton = document.getElementById('channelSetupButton');
const twitchInput = document.getElementById('channelSetupInput');
const channelsList = document.getElementById('channelList')

let socket = io()

socket.on('connect', function() {
    socket.emit('connect_response', 'client socket connected')
})

socket.on('welcome_msg', function(data) {
    console.log(data);
})

function addLiChannel (channelName) {

    let li = document.createElement('li');
    li.setAttribute('id', channelName.toLowerCase())

    let p = document.createElement('p');

    p.innerText = channelName;
    p.innerHTML = channelName;

    let disconnectButton = document.createElement('button');
    disconnectButton.innerText = 'Disconnect';
    disconnectButton.innerHTML = 'Disconnect';
    disconnectButton.setAttribute("id", "channelDisconnect");

    disconnectButton.onclick = disconnect;


    li.appendChild(p);
    li.appendChild(disconnectButton);

    channelList.appendChild(li);
    console.log(channelList);
};

twitchInput.addEventListener('keydown', function (event) {
    if (event.key == "Enter") {
        event.preventDefault(); //Say event is done
        twitchButton.click();
    }
})

// add channel trougth socket.io
socket.on('channel_added', function(data) {
    // data format: {added: <true/false>, channel: <channelName>}

    if (data.added === true) {
        addLiChannel(data.channel);
        console.log(data);
    } else if (data.added === false) {
        alert('Error occured when attempt to connect to Twitch channel');
    }
})

twitchButton.onclick = function () {

    const channel = twitchInput.value;
    twitchInput.value = "";

    if (channel != "") {
        socket.emit('add_channel', {channel: channel})
        }
}

// remove li tag if channel is diconnected
socket.on('channel_disconnected', function(data) {
    // data format: {channel: <channelName>}

    let del = document.getElementById(data.channel);
    del.remove();

})

function disconnect() {

    let channel = this.previousSibling.innerText.toLowerCase();

    socket.emit('disconnet_channel', {channel: channel})

    // url = '/api/twitch/twitch_disconnect'
    // + '?channel='
    // + this.previousSibling.innerText.toLowerCase();
    //
    // let xhttp = new XMLHttpRequest();
    // xhttp.onreadystatechange = function() {
    //     if (this.readyState == 4 && this.status == 200) {
    //         let response = JSON.parse(this.response);
    //
    //         if (response.disconnected === true) {
    //             let del = document.getElementById(response.channel);
    //             del.remove();
    //         } else {
    //             console.log('An error has occured');
    //         }
    //     } else if (this.readyState == 4 && this.status != 200) {
    //         twitchInput.placeholder = "An error has occured, try agin.";
    //     }
    // }
    //
    // xhttp.open('GET', url);
    // xhttp.send();
}
