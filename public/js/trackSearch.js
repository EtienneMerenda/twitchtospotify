// Button search

const spotifySearch = document.getElementById('spotify_search')
const spotifySearchTxt = document.getElementById('spotifySearchTxt')

// spotifySearch button action
spotifySearch.onclick = function(e) {
    //get query
    let query = spotifySearchTxt.value;
    getMusicList(query);
}

// Add response in div !!

const spotify_response = document.getElementById('response')

function getMusicList(query) {
    let xhttp = new XMLHttpRequest();
        url = '/api/music?q=' + query + '&type=artist,album,tarck'

    // Define what action doing when state change.

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(JSON.parse(this.response));

            let music = JSON.parse(this.response);

            // EXTRACT VALUE FOR HTML HEADER.
            // ('Book ID', 'Book Name', 'Category' and 'Price')
            let col = [];
            for (let i = 0; i < music.length; i++) {
                for (let key in music[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                        console.log(key);
                    }
                }
            }

            // CREATE DYNAMIC TABLE.
            let table = document.createElement("table");

            // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

            let tr = table.insertRow(-1);                   // TABLE ROW.

            for (let i = 0; i < col.length; i++) {
                let th = document.createElement("th");      // TABLE HEADER.
                th.innerHTML = col[i];
                tr.appendChild(th);
            }

            // ADD JSON DATA TO THE TABLE AS ROWS.
            for (let i = 0; i < music.length; i++) {

                tr = table.insertRow(-1);

                for (let j = 0; j < col.length; j++) {
                    let tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = music[i][col[j]];
                    if (j == 3) {
                        tabCell.className = "trackUri";
                    }
                }
            }

            // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
            let divContainer = document.getElementById("showData");
            divContainer.innerHTML = "";
            divContainer.appendChild(table);

            // add addEventListener on uriClick
            const uris = document.getElementsByClassName('trackUri');
            for (let i = 0; i < uris.length; i++) {
                uris[i].addEventListener('click', addTrack);
                };

        } else if (this.readyState == 4 && this.status != 200){
            console.log('AJAX error')
        };
    };

    xhttp.open('GET', url);
    xhttp.send();
};
