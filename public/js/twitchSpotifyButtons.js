const spotify = document.getElementById('spotify_button');
const twitch = document.getElementById('twitch_button');

// Adapative message on Spotify button

if (window.location.hash === "#error=invalid_token"
    || window.location.hash === "#error=state_mismatch"
    || window.location.hash === "") {

    spotify.innerHTML = "Connect to Spotify"
} else {
    spotify.innerHTML = "Already connected to Spotify !"
}

// onclick function

spotify.onclick = function() {window.location.replace('/login_spotify');
};
twitch.onclick = function() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log('Réussite !');
            console.log(this);
        } else {
            console.log('error');
            console.log(this);
        }
    };
    xhttp.open('GET', 'api/twitch_login')
    xhttp.send()
};
