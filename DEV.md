About applications data:

tools in \_:

{
  spotify: {
    stateKey: 'spotify_auth_state',
    getAccessToken: [Function: getAccessToken],
    getUserId: [Function: getUserId],
    loginRoute: [Function: loginRoute],
    callbackRoute: [Function: callbackRoute],
    getPlaylist: [Function: getPlaylist],
    createPlaylist: [Function: createPlaylist],
    getTracks: [Function: getTracks],
    addTrack: [Function: addTrack]
  },
  twitch: {
    loginRoute: [Function: loginRoute],
    disconnectRoute: [Function: disconnectRoute],
    getChannels: [Function: getChannels],
    generateOptions: [Function: generateOptions],
    connect: [Function: connect],
    disconnect: [Function: disconnect],
    onMessageHandler: [Function: onMessageHandler],
    onConnectedHandler: [Function: onConnectedHandler]
  },
  tools: {
    express: [Function: createApplication],
    request: [Function: request],
    cors: [Function: middlewareWrapper],
    querystring: [Function: querystring],
    cookieParser: [Function: cookieParser],
    bodyParser: [Function bodyParser],
    tmi: [Function: tmi],
    randomString: [Function: randomString],
  },
  indexRoute: [Function: indexRoute],
  user: {
    clientId: **************,
    clientSecret: ***************,
    redirectUri: *****************
  }
}

\_.search = {userName: {
                        'tracks': {
                                    1: [trackName, uri],
                                    2: [trackName, uri]
                                  },
                        'Date': Date.now()
                       }
          }
