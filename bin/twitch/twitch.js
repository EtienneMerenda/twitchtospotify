const onConnectedHandler = require('./function/onConnectedHandler').onConnectedHandler
const onMessageHandler = require('./function/onMessageHandler').onMessageHandler

const generateOptions = require('./function/connect').generateOptions
const connect = require('./function/connect').connect
const disconnect = require('./function/connect').disconnect

const sayToClient = require('./function/sayToClient').sayToClient



module.exports = {
    generateOptions: generateOptions,
    connect: connect,
    disconnect: disconnect,
    onMessageHandler: onMessageHandler,
    onConnectedHandler: onConnectedHandler,
    sayToClient: sayToClient
}
