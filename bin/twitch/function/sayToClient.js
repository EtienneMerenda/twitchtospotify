// function used to send response to client
function sayToClient(target, msg) {
    const say = _.twitch.client.say
    say(target, msg)
}

module.exports.sayToClient = sayToClient;
