function connect(channel){

    let opts = _.twitch.generateOptions(channel)

    _.twitch.client = new _.tools.tmi.client(opts);
    _.twitch.client.on('message', _.twitch.onMessageHandler);
    _.twitch.client.on('connected', _.twitch.onConnectedHandler);
    let promise = _.twitch.client.connect()

    return promise

};

function generateOptions(channels_list) {
    let opts = {
      identity: {
        username: 'SpotiBot',
        password: 'oauth:8ldrsnrn33f5bdmzijk0sq5xojonnu'
      },
      channels: channels_list
    };

    return opts
};

function disconnect() {
    _.twitch.client.disconnect();
    _.twitch.client = null;

};

module.exports = {
    connect: connect,
    generateOptions: generateOptions,
    disconnect: disconnect
};
