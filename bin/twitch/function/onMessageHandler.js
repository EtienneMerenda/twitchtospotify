// Called every time a message comes in
function onMessageHandler (target, context, msg, self) {

    const spotify = _.spotify;
    const music = _.music;
    const twitch = _.twitch

    const userName = context.username

    if (self) { return; } // Ignore messages from the bot

    // Remove whitespace from chat message
    msg = msg.trim();

    let command = msg.substring(0, 3)

    // If the command is known, let's execute it


    if (command === "!sr") {

        // If msg bigin with '!t', get track query
        const query = msg.substring(4, msg.length)

        // Get tracks on Spotify
        spotify.getTracks(query)

        // Catch error on getTracks
        .catch((err) => {console.log(err);})

        // Handling search result
        .then(music.searchResultHandler)

        // Catch error on searchResultHandler
        .catch((err) => {
            console.log(err);
            let msg = `Song ${query} not found.`
            twitch.client.say(target, msg)
        })

        // Finaly
        .then((data) => {
            console.log('final log');
            console.log(data);

            if (data.add === true) {
                spotify.addTrack(data.musics.uri)
                twitch.client.say(target, data.msg);
            } else {
                for (let msg of data.msg) {
                    twitch.client.say(target, msg);
                };
                _.music.queryHandler[userName] = data.musics
                console.log(_.music.queryHandler);
                console.log(_.music.queryHandler.length);
            }
        })
        console.log(`* Executed "${msg}" command`);

    } else if (command === "!sl" && Object.keys(_.music.queryHandler).length > 0) {

        console.log(_.music.queryHandler[userName]);
        let oldSearch = _.music.queryHandler[userName]

        let query_ = msg.substring(3, msg.length);
        query_ = Number(query_) - 1;

        console.log(oldSearch[query_]);

        if ([...Array(oldSearch.length).keys()].includes(query_)) {
            let trackName = oldSearch[query_].name;
            let uri = oldSearch[query_].uri;
            let msg = `Song '${trackName}' added`;

            console.log(trackName);
            console.log(uri);
            console.log(oldSearch);

            spotify.addTrack(uri)
            .then(() => {
                twitch.client.say(target, msg);
            })
            .catch((err) => {console.log(err);})
        };
    } else {
        console.log(`* Unknown command "${msg}"`);
    }

}

module.exports.onMessageHandler = onMessageHandler;
