// Import all module and export to TwitchToSpotify script

const getAccessToken = require('./function/getAccessToken').getAccessToken
const getUserId = require('./function/getUserId').getUserId

const loginRoute = require('./route/loginSpotify').loginRoute
const callbackRoute = require('./route/callbackSpotify').callbackRoute

const getPlaylist = require('./function/getPlaylist').getPlaylist
const createPlaylist = require('./function/createPlaylist').createPlaylist

const getTracks = require('./function/getTracks').getTracks
const addTrack = require('./function/addTrack').addTrack

const stateKey = 'spotify_auth_state';

module.exports = {
    stateKey: stateKey,

    getAccessToken: getAccessToken,
    getUserId: getUserId,

    loginRoute: loginRoute,
    callbackRoute: callbackRoute,

    getPlaylist: getPlaylist,
    createPlaylist: createPlaylist,

    getTracks: getTracks,
    addTrack: addTrack,
};
