// request for user authorization function.
function loginRoute (app) {

    app.get('/login_spotify', function(req, res) {

        const state = _.tools.randomString(16);
        const stateKey = _.stateKey

        const userId = _.user.clientId
        const redirectUri = _.user.redirectUri

        res.cookie(stateKey, state);

        // your application requests authorization
        const scope = 'playlist-modify-public ';

        res.redirect('https://accounts.spotify.com/authorize?' +
            _.tools.querystring.stringify({
            response_type: 'code',
            client_id: userId,
            scope: scope,
            redirect_uri: redirectUri,
            state: state
        }));
    });
};

module.exports.loginRoute = loginRoute;
