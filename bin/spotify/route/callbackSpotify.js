// Callback of user authorization request.
function callbackRoute (app) {

    app.get('/callback_spotify', function(req, res) {

        _.user.code = req.query.code

        const spotify = _.spotify

        // your application requests refresh and access tokens
        // after checking the state parameter

        let code = req.query.code || null;
            state = req.query.state || null;
            storedState = req.cookies ? req.cookies[_.stateKey] : null;

        // If state is null => error
        if (state === null || state !== storedState) {
            res.render('index.ejs', {
                text: "Erf ! Something was wrong, try agin !"
            })
        } else {

            let success = true

            // Get token
            spotify.getAccessToken()
                .then(spotify.getUserId)
                .catch((err) => {success = false; console.log(err);})
                .then(spotify.getPlaylist)
                .catch((err) => {success = false; console.log(err);})
                .then(function () {

                    if (success) {
                    // Add cookie to say app is ready to work
                        res.cookie('spotifyLogin', 'true')
                        res.redirect('/app')
                    } else {
                        console.log('Failed to log');
                        res.redirect('/')
                    }
                })
        }
    })
}

module.exports.callbackRoute = callbackRoute;
