// Get and setup playlist if not already created
function getPlaylist() {

    return new Promise(function(resolve, reject) {

        const request = _.tools.request
        const token = _.user.accessToken
        const spotify = _.spotify

        // Generate option for get playlist list
        const options = {
          url: 'https://api.spotify.com/v1/me/playlists',
          headers: { 'Authorization': 'Bearer ' + token },
          json: true
        };

        // console.log(options);

        request.get(options, function(error, response, body) {

            // Create boolean for TwitchToSpotify playlist
            let playlists = body.items
            let twitchToSpotifyPlaylist = false;

            // loop on each playlist to found appropriate playlist
            for (let i = 0; i < playlists.length; i++) {
                if (playlists[i].name == "TwitchToSpotify") {
                    _.user.playlistId = playlists[i].id;
                    twitchToSpotifyPlaylist = true;
                    resolve("Playlist founded.")
                };
            };

            // If playlist not already exist, create it.
            if (twitchToSpotifyPlaylist == false) {
                spotify.createPlaylist().then(resolve("playlist created."))
            }
        });
    });
};

module.exports.getPlaylist = getPlaylist;
