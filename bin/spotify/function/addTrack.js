// Used to add track on Spotify Playlist
function addTrack (uri) {

    /* data format: {
                    name: '<trackName>',
                    uri: '<trackUri>'
                    }
    */

    return new Promise(function(resolve, reject) {
        console.log('data');
        console.log(data);

        // Define functions
        const request = _.tools.request;

        // Define data
        const user = _.user;
        const trackName = data.name;

        // Setup options of request for Spotify
        const options = {
            url: 'https://api.spotify.com/v1/users/' + user.id + '/playlists/' + user.playlistId + '/tracks',
            headers: { 'Authorization': 'Bearer ' + user.accessToken },
            json: true,
            body: {"uris": [uri]}
            };

            console.log(options);

        // Post request
        request.post(options, function(error, response, body) {
            if (error) {
                console.log(error);
                reject(error)
            } else {
                if (body.error) {
                    reject(body.error)
                } else {
                    console.log(response);
                    resolve(`Track ${data.name} succefully added.`)
                }
            };
        });
    });
};

module.exports.addTrack = addTrack;
