// Create TwitchToSpotify playlist if doesn't exist
function createPlaylist() {

    return new Promise(function(resolve, reject) {

        const request = _.tools.request;

        const userId = _.user.id;
        const token = _.user.accessToken;

        // Create request to create playlist
        const options = {
          url: 'https://api.spotify.com/v1/users/' + userId + '/playlists',
          headers: { 'Authorization': 'Bearer ' + token },
          body: {
              "name": "TwitchToSpotify",
              "description": "Playlist used on my stream",
              "public": true
              },
          json: true
        };

        request.post(options, function(error, response, body) {
            if (error) {console.log(error);reject('Failed to create playlist.')}
            else {resolve("Playlist created.")}
        });

    });
}

module.exports.createPlaylist = createPlaylist;
