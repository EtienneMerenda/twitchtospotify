// Search tracks match the query
function getTracks (query) {

    /* data format: {
           user: '<userName>'
           query: '<userQuery>'
       }
    */

    function generateItemsList(body) {

        /*output format: {
                            name:
                            artist:
                            album:
                            uri:
                        }

        */

        musics = []

        // loop on each tracks
        for (i in body.tracks.items) {

            const musicRaw = body.tracks.items[i]

            // get track name, artists, album and uri
            let musicsData = {
                name: musicRaw.name,
                artist: [],
                album: musicRaw.album.name,
                uri: musicRaw.uri
                };

            // loop on each artist and add him in artist list
            for (let i in musicRaw.artists) {
                musicsData.artist.push(musicRaw.artists[i].name);
                };

            //Stringify artist list
            musicsData.artist = musicsData.artist.join(", ")

            musics.push(musicsData)
            }
        return musics
    }

    return new Promise(function(resolve, reject) {

        // Define function
        const request = _.tools.request;

        // Define data
        const token = _.user.accessToken;

        // Define options
        const options = {
          url: 'https://api.spotify.com/v1/search?q='+query+'&type=track&limit=5',
          headers: { 'Authorization': 'Bearer ' + token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
            if (error) {
                console.log('getTracks request error');
                reject(error);
            } else {
                if (body.error) {
                    console.log('getTracks body error');
                    reject(body.error);
                } else {
                    console.log('getTracks success');
                    // `return` dict of tracks found
                    resolve(generateItemsList(body));
                }
            }
        });
    });
};

module.exports.getTracks = getTracks;
