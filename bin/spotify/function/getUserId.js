// function used for get user_id
function getUserId () {

    return new Promise(function(resolve, reject) {

        const request = _.tools.request
        const token = _.user.accessToken

        // Generate option for get userID
        const options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
            if (error) {
                reject("! Failed to get user id.");
            } else {
                _.user.id = body.id;
                resolve("User id founded.");
            }
        });
    });
};

module.exports.getUserId = getUserId;
