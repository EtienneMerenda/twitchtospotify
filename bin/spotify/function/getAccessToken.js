function getAccessToken () {
    // Request for token to exchange with API of Spotify.
    return new Promise(function(resolve, reject) {

        // define data
        const stateKey = _.spotify.stateKey
        const redirectUri = _.user.redirectUri

        const clientId = _.user.clientId
        const clientSecret = _.user.clientSecret
        const code = _.user.code

        // define tools
        const request = _.tools.request
        const spotify = _.spotify

        const authOptions = {
            url: 'https://accounts.spotify.com/api/token',
            form: {
                code: code,
                redirect_uri: redirectUri,
                grant_type: 'authorization_code'
                },
            headers: {
                'Authorization': 'Basic ' + (new Buffer.from(clientId + ':' + clientSecret).toString('base64'))
                },
            json: true
            };

        // post request to get accesToken and refreshtoken
        request.post(authOptions, function(error, response, body) {
            if (!error && response.statusCode === 200) {

                _.user.accessToken = body.access_token,
                _.user.refreshToken = body.refresh_token;
                resolve("Acces token and refresh token getted.")
            } else {
                reject("Error to get token and refresh token.")
            }
        });
    })
}


module.exports.getAccessToken = getAccessToken;
