const spotify = require('./spotify/spotify')
const twitch = require('./twitch/twitch')
const tools = require('./tools/tools')
const appRoute = require('./app/app')
const music = require('./music/music')
const socket = require('./socket/socket')

module.exports = {
    spotify: spotify,
    twitch: twitch,
    tools: tools,
    appRoute: appRoute,
    music: music,
    socket: socket
}
