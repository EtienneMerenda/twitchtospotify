// Import all utilities modules

// Web server framework
const express = require('express'); // Express web server framework
// "Request" library
const request = require('request');


// Web socket for real time exchange with app
const http = require('http')
const io = require('socket.io');

// Middlewares
const cors = require('cors');
const querystring = require('querystring'); // middleware to change objenct in URL query
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser'); // for get post request

// Twitch package
const tmi = require('tmi.js'); // Twitch module


const randomString = require('./randomString').randomString

module.exports = {
    express: express,
    request: request,
    cors: cors,
    querystring: querystring,
    cookieParser: cookieParser,
    bodyParser: bodyParser,
    tmi: tmi,
    randomString: randomString,
    io: io,
    http: http
}
