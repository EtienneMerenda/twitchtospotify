function searchResultHandler(musics) {

    return new Promise(function(resolve, reject) {

        /* return json :
        {added: <true/false>,
        musics: musics
        msg:}
        */

        console.log('search handler');
        console.log(musics);
        console.log(musics.length);
        console.log('------------------------------------------------');

        const spotify = _.spotify;
        const music = _.music;

        let msg = ""

        music.purgeOldSearch()

        switch (musics.length) {
            // If list is empty, do nothing
            case 0:
                console.log("no music found");
                reject("no music found")
                break;

            // If list contains one item, addTrack to playlist
            case 1:
                msg = `Song '${musics.musicList[0].name}' added`
                data = {add: true, musics: musics[0], msg: msg};
                resolve(data);

                break;

            // Else add search result on dict and wait for user response
            default:

                msg = [`Choose whith !sl <track number>`];

                for (let i=0; i<musics.length; i++) {
                    let name = musics[i].name;
                    let artist = musics[i].artist;
                    msg.push(`${i+1}: ${name} of ${artist}`);
                };

                data = {added: false, musics: musics, msg: msg};
                console.log(data);
                resolve(data);
                }
        }
    );
}

module.exports.searchResultHandler = searchResultHandler;
