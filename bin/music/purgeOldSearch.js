// remove search data have more than 10 minutes
function purgeOldSearch() {

    if (_.music.queryHandler.length > 0) {

        for (const data of _.music.queryHandler) {
            console.log(data);
            if (Date.now() - 1000 * 60 * 10 > data.date ) {
                delete _.queryHandler[data];
            }

        }
    }
}

module.exports.purgeOldSearch = purgeOldSearch;
