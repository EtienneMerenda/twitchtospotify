const searchResultHandler = require('./searchResultHandler').searchResultHandler
const purgeOldSearch = require('./purgeOldSearch').purgeOldSearch

module.exports = {
    purgeOldSearch: purgeOldSearch,
    searchResultHandler: searchResultHandler,
    queryHandler: {}
}
