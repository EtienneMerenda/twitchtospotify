function appRoute (app) {
    app.get('/app', function(req, res) {

        const playlistId = _.user.playlistId;

        // If spotifyLogin cookie exist and is true, return run page
        if (req.cookies.spotifyLogin == 'true') {
            res.render('app.ejs', {
                playlist_id: playlistId
            })
        // Else, come back to main page
        } else {
            res.render('index.ejs', {
                text: 'You must log on Spotify for before acces run page !'
            });
        }


    })

    // Setting main route
    app.get('/', function(req, res) {
        res.render('index.ejs', {
            text: 'We need to log in Spotify for begin !'
        });
    });
};


module.exports.appRoute = appRoute;
