function setupOnEvent(io) {

    // When user connect, bind all event for app
    io.on('connect', function (socket) {
        // setup response for connect response of cient side
        socket.on('connect_response', function(data) {console.log(data)})
        socket.emit('welcome_msg', 'You are log on TwitchToSpotify socket server', function() {
        })



        // Twitch on event --------------------------

        socket.on('add_channel', function(data) {
            // data incoming: {channel: <channelName>}

            // Get tools
            const generateOptions = _.twitch.generateOptions;
            const twitch = _.twitch;

            // Get data
            const channel = data.channel;

            // First connection
            if (twitch.client === undefined) {

                let promise = twitch.connect([channel])

                promise.then(function() {
                    socket.emit('channel_added', {added: true,
                                                  channel: channel}
                    )});

                promise.catch('channel_added', function() {
                    socket.emit('channel_added', {added: false,
                                                  channel: channel}
                    )});



            // Verify channels already listen, add or not new channel.
            } else {

                const channels = twitch.client.getChannels();

                if (channels.includes('#'+channel)) {
                    console.log('channel already added');
                } else {

                    twitch.disconnect();
                    channels.push(channel);
                    let promise = twitch.connect(channels);

                    promise.then(function() {
                        socket.emit('channel_added', {added: true,
                                                      channel: channel}
                        )});

                    promise.catch('channel_added', function() {
                        socket.emit('channel_added', {added: false,
                                                      channel: channel}
                        )});
                }
            }
        })

        socket.on('disconnet_channel', function(data) {
            // data incoming: {channel: <channelName>}

            const twitch = _.twitch;

            let channel = '#' + data.channel;
            let channels = twitch.client.getChannels();

            if (channels.includes(channel)) {
                channels = channels.filter(e => e !== channel)

                twitch.disconnect()

                twitch.connect(channels)

                socket.emit(
                    'channel_disconnected',
                    {channel: channel.substring(1, channel.length)}
                );
            }
        })


    })
}

module.exports.setupOnEvent = setupOnEvent;
